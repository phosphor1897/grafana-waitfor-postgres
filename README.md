# grafana-waitfor-postgres

docker container that acts exactly like the official grafana one except that it waits for postgres

it does not parse or use GF_DATABASE_URL, so make sure to set the individual ones
